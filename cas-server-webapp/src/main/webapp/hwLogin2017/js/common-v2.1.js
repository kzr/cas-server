/**
 * Created by DELL on 2017/7/5.
 */
// 城市选择
$('.dropdownMenus.citySelectMenus>a').on('click', function() {
    var $this = $(this);
    var text = $this.text();
    $this.parent().prev().children('b').text(text);
});

// 客服热线
$('.rightBar .telephone').on('mouseover', function() {
    $(this).find('.showBox').show();
});
$('.rightBar .telephone').on('mouseleave', function() {
    $(this).find('.showBox').hide();
});

// 滚动
$(window).scroll(function() {
    var top = $(this).scrollTop();
    if(top > 500) {
        $('.rightBar').show();
    } else {
        $('.rightBar').hide();
    }
});

// 回到顶部
$('.rightBar>div.top').on('click', function() {
    $('html,body').animate({scrollTop: 0}, 400);
});

function resetHeight() {

    // 容器高度
    var containerHeight = $('#fullScreenContainer').height();
    var contentHeight = $('.fullScreenContent').height();
    var sideBarHeight = $('.fullScreenSideBar').height();

// 屏幕可见区域高度
    var clientHeight = document.body.clientHeight;
    var scrollHeight = document.body.scrollHeight;
    if(sideBarHeight < clientHeight || sideBarHeight < contentHeight) {
        $('.fullScreenSideBar').css({position: 'absolute'});
    }
}

resetHeight();

