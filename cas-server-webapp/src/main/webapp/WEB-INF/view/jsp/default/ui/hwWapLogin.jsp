<%@ page contentType="text/html; charset=UTF-8" %>
<%
  String path = request.getContextPath();
  request.setAttribute("root",path);
  request.setAttribute("ticket_root","http://m.zhihuihuiwu.com");
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="utf-8" />
  <meta name="format-detection" content="telephone=no">
  <meta content="yes" name="apple-mobile-web-app-capable">
  <meta content="black" name="apple-mobile-web-app-status-bar-style">
  <link rel="icon" href="${root}/hwLogin/images/zhhw.ico" type="image/x-icon">
  <title>智慧会务-用户登录</title>
  <script>;(function(){var W=640,w=parseInt(window.screen.width),s=w/W,u=navigator.userAgent.toLowerCase(),m='<meta name="viewport" content="width='+W+',';if(/android (\d+\.\d+)/.test(u)){if(parseFloat(RegExp.$1)>2.3)m+='minimum-scale='+s+',maximum-scale='+s+',';}else{m+='user-scalable=no,';}m+='target-densitydpi=device-dpi">';document.write(m);}());</script>
  <link href="${root}/hwWapLogin/css/conference.css" rel="stylesheet" type="text/css">
  <style type="text/css">
    /*html5*/
    article,aside,dialog,footer,header,section,footer,nav,figure,menu{display:block}
    .registerBox { padding: 50px 37px 0 37px;}
    .registerBox li { padding-left: 38px;border-bottom: 1px solid #e5e5e5!important;}

    .registerBox li span { left: 0;}
  </style>
  <!--[if lt IE 9]>
  <script type="text/javascript">
    var e = "abbr, article, aside, audio, canvas, datalist, details, dialog, eventsource, figure, footer, header, hgroup, mark, menu, meter, nav, output, progress, section, time, video".split(', ');
    var i= e.length;
    while (i--){
      document.createElement(e[i])
    }
  </script>
  <![endif]-->
</head>
<body>
<form:form method="post" id="loginForm" commandName="${commandName}" htmlEscape="true" novalidate="novalidate" autocomplete="off">
<form:errors path="*" id="errorMsg" cssClass="errors" cssStyle="display: none;" element="div" htmlEscape="false" />
<input type="hidden" name="lt" value="${loginTicket}" />
<input type="hidden" name="execution" value="${flowExecutionKey}" />
<input type="hidden" name="_eventId" value="submit" />
<div class="registerBox">
  <ul>
    <li>
      <span></span>
      <input name="username" id="username" type="text" class="v_ipt" placeholder="手机号" maxlength="64">
    </li>
    <li>
      <span class="v_p"></span>
      <input name="password" id="password" type="password" class="v_ipt" placeholder="输入密码" maxlength="32">
    </li>
    <input name="rememberMe" id="rememberMe" value="true" type="hidden" checked="checked">
    <input name="yzm" id="yzm" value="true" type="hidden" checked="checked">
  </ul>
  <p id="submitButton" class="getBtn_SetUp mr_t_80">登录</p>
  <p class="text-r regi_ps"><a href="${ticket_root}/user/forget">忘记密码?</a> </p>
</div>
<div class="thirtyBox">
  <dl>
    <dt><span>使用其他账号登录</span></dt>
    <dd>
      <a href="${WeiXinClientUrl}" id="weiXinClientUrl">
        <p><img src="${root}/hwWapLogin/images/icon_wechat@2x.png"/> </p>
        <span>微信</span>
      </a>
      <a href="${QqClientUrl}">
        <p><img src="${root}/hwWapLogin/images/icon_qq@2x.png"/></p>
        <span>QQ</span>
      </a>
      <a href="${WeiboClientUrl}">
        <p><img src="${root}/hwWapLogin/images/icon_weibo@2x.png"/> </p>
        <span>微博</span>
      </a>
    </dd>
  </dl>
</div>
</form:form>
<p class="reg_bot_Box" style="padding-top: 100px;">这些账号都没有，<a href="${ticket_root}/user/register">我要注册</a></p>
<div id="tips" class="tipsBox">请输入正确的手机号</div>
<script src="${root}/hwWapLogin/js/jquery-1.10.2.min.js"></script>
<script src="${root}/hwWapLogin/js/login.js"></script>

</body>
</html>