<%@ page contentType="text/html; charset=UTF-8" %>
<%
  String path = request.getContextPath();
  request.setAttribute("root",path);
  request.setAttribute("sf_root","http://local.zhhw.com/ticket");
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <title>智慧会务</title>
  <meta name="keywords" content="让您的会务更简单,华阳信通"/>
  <meta name="description" content="智慧会务" />
  <link rel="icon" href="${root}/hwLogin/images/zhhw.ico" type="image/x-icon">
  <link href="${root}/hwLogin/css/global.css?v=20151021" rel="stylesheet" type="text/css" />
  <link href="${root}/hwLogin/css/register.css?v=20151021" rel="stylesheet" type="text/css" />
  <link href="${root}/hwLogin/css/jquery.placeholder.css?v=20151021" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="index-header b-1-bot">
  <dl>
    <dt class="f-left"><a href="${sf_root}"><img src="${root}/hwLogin/images/logo.png?v=20151021" class="f-left mt-12"/></a></dt>
    <dd class="f-right mt-14">
      <a href="${sf_root}" class=" getReturnHome">返回首页<em></em></a>
    </dd>
  </dl>
</div>
<input id="sf_root" type="hidden" value="${sf_root}">
<input id="bm_root" type="hidden" value="${bm_root}">
<div class="smallBox pb-30">
  <form:form method="post" id="loginForm" commandName="${commandName}" htmlEscape="true" novalidate="novalidate" autocomplete="off">
    <form:errors path="*" id="errorMsg" cssClass="errors" cssStyle="display: none;" element="div" htmlEscape="false" />
    <input type="hidden" name="lt" value="${loginTicket}" />
    <input type="hidden" name="execution" value="${flowExecutionKey}" />
    <input type="hidden" name="_eventId" value="submit" />
  <dl>
    <dt class="pt-30">用户登录</dt>
    <dd>
      <ul>
        <li style="display: none;margin-top: -12px"><p id="serverErrorTips" class="errorTips"><em></em>请输入正确的密码</p></li>
        <li>
          <span class="s-icon s-icon-login"></span>
          <input name="username" id="username" class="s-ipt w-453"  type="text" placeholder="请输入手机号或账户名"/>
          <p id="usernameTip" class="errorTips"><em></em>请输入手机号或账户名</p>
        </li>
        <li>
          <span class="s-icon s-i-psw"></span>
          <input name="password" id="password" class="s-ipt w-453"  type="password" placeholder="请输入密码"/>
          <p id="passwordTip" class="errorTips"><em></em>请输入正确的密码</p>
        </li>
        <li>
          <span class="s-i-psw"></span>
          <input name="yzm" id="code" class="s-ipt w-250"  type="text" placeholder="请输入验证码" style="text-indent: 10px;"/>
          <img id="yzmImg" class="f-right" src="captcha.jpg" style="cursor: pointer;"  height="55" alt="验证码" onclick="refresh()">
          <p id="codeTip" class="errorTips"><em></em>请输入正确的验证码</p>
        </li>
        <li>
          <span class="f-left remember"><em ><input name="rememberMe" id="rememberMe" value="true" type="radio" checked="checked"></em>记住我</span>
        </li>
      </ul>
      <h3 class="mt-33"><input id="submitButton" type="button" onclick="verSubmit();" value="登 录" class="getRegBnt"></h3>
      <p class="s-txt"><span>还未注册</span></p>
      <a href="${sf_root}/register" class="getLogin">马上前往注册<em></em></a>
    </dd>
  </dl>
  </form:form>
</div>

<a href="${WeiXinClientUrl}" id="weiXinClientUrl">微信登录</a> <br />
<a href="${WeiboClientUrl}">微博登录</a> <br />
<a href="${QqClientUrl}">QQ登录</a> <br />

<div class="smFooter"><p>版权所有 2015深圳启会科技发展有限公司</p></div>
<script src="${root}/hwLogin/js/jquery.min.js?v=20151021"></script>
<script src="${root}/hwLogin/js/login.js?v=20160811"></script>
<script>
  function isWeiXin(){
    var ua = window.navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
      return true;
    }else{
      return false;
    }
  };
  $(function(){
    if(!isWeiXin()){
      var href = $('#weiXinClientUrl').attr('href').replace(/oauth2\/authorize/g,'qrconnect');
      $('#weiXinClientUrl').attr('href',href);
    }
  })
</script>
</body>
</html>
