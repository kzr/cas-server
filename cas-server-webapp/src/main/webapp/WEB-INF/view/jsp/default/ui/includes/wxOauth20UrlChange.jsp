<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script>
  function isWeiXin(){
    var ua = window.navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
      return true;
    }else{
      return false;
    }
  };
  if(!isWeiXin()){
    var href = $('#weiXinClientUrl').attr('href').replace(/oauth2\/authorize/g,'qrconnect');
    $('#weiXinClientUrl').attr('href',href);
  }
</script>
