<%@ page contentType="text/html; charset=UTF-8" %>
<%
  String path = request.getContextPath();
  request.setAttribute("root",path);
//  request.setAttribute("sf_root","http://192.168.10.199/ticket");
  request.setAttribute("sf_root","http://www.zhihuihuiwu.com");
%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <title>智慧会务</title>
  <meta name="keywords" content="让您的会务更简单"/>
  <meta name="description" content="智慧会务" />
  <link rel="stylesheet" href="${root}/hwLogin2017/css/pc-redesign-css-201706.css?20170922"/>
  <style type="text/css">
    .m-list li a h3{ color: #202020; margin-bottom: 20px; font-size: 16px; height: auto; line-height: normal; overflow: auto; }
    .m-list li a p.time{ margin-bottom: 24px; }
    .m-list li a p.time,.m-list li a p.desc{ color: #6f6f6f; font-size: 14px; }
    .s-ipt{ text-indent: 45px; }
  </style>
</head>
<body style="overflow-x: hidden;background-color: #f7f8fb;">
<div class="index-header ">
  <dl>
    <dt class="f-left" ><img src="${root}/hwLogin2017/images/index/logo.png" class="f-left mt-12"/><span class="tp-txt f-left"></span></dt>
      <dd class="f-left mt-14 mr-40">
                    <span class="hoverdown">
                        <a class="parent" href="javascript:;"><b>
                            全国
                        </b><i class="arrow"></i></a>
                        <span class="dropdownMenus citySelectMenus" >
                            <p class="title">热门</p>
                            <a href="${sf_root}/">全国</a>
                            <a href="${sf_root}/shenzhen">深圳</a>
                            <a href="${sf_root}/beijing">北京</a>
                            <a href="${sf_root}/shanghai">上海</a>
                            <a href="${sf_root}/guangzhou">广州</a>
                            <a href="${sf_root}/hangzhou">杭州</a>
                        </span>
                    </span>
      </dd>
      <dd class="f-left mt-14 mr-20">
                    <span>
                        <a href="${sf_root}/">首页</a>
                    </span>
      </dd>
      <dd class="f-left mt-14 mr-20">
                    <span>
                        <a href="${sf_root}/tv" >会议直播</a>
                    </span>
      </dd>
      <dd class="f-left mt-14 mr-20">
                    <span class="hoverdown">
                        <a class="parent" href="${sf_root}/eventmanage">会议管理<i class="arrow"></i></a>
                        <span class="dropdownMenus" >
                            <a href="${sf_root}/eventmanage#2">会议创建</a>
                            <a href="${sf_root}/eventmanage#3">注册报名</a>
                            <a href="${sf_root}/eventmanage#4">渠道推广</a>
                            <a href="${sf_root}/eventmanage#5">报名管理</a>
                            <a href="${sf_root}/eventmanage#6">数据统计</a>

                        </span>
                    </span>
      </dd>
      <dd class="f-left mt-14 mr-20">
                    <span class="hoverdown">
                        <a class="parent" href="${sf_root}/esign">电子签到<i class="arrow"></i></a>
                        <span class="dropdownMenus" >
                            <a href="${sf_root}/esign#2">自助签到</a>
                            <a href="${sf_root}/esign#4">微信签到</a>
                            <a href="${sf_root}/esign#4">POS签到</a>
                            <a href="${sf_root}/esign#5">现场制证</a>
                        </span>
                    </span>
      </dd>
      <dd class="f-left mt-14 mr-20">
                    <span class="hoverdown">
                        <a class="parent" href="${sf_root}/hd">3D互动<i class="arrow" style="display: none;"></i></a>
                    </span>
      </dd>
      <dd class="f-right mt-14 mr-20 " style="display: block;">
        <span>
            <a id="login" href="#" >登录</a>
        </span>
      </dd>
      <dd class="f-right mt-14 mr-20" style="display: block;">
        <span>
            <a href="${sf_root}/user/register" >注册</a>
        </span>
      </dd>
  </dl>
</div>
<form:form method="post" id="loginForm" commandName="${commandName}" htmlEscape="true" novalidate="novalidate" autocomplete="off">
    <form:errors path="*" id="errorMsg" cssClass="errors" cssStyle="display: none;" element="div" htmlEscape="false" />
    <input type="hidden" name="lt" value="${loginTicket}" />
    <input type="hidden" name="execution" value="${flowExecutionKey}" />
    <input type="hidden" name="_eventId" value="submit" />
<div class="pc-login-register-container">
  <div class="smallBox pb-30">
    <dl>
      <dt class="pt-30">用户登录</dt>
      <dd>
        <ul>
          <li>
            <span class="s-icon s-icon-login"></span>
            <input class="s-ipt" name="username" id="username" type="text" placeholder="请输入手机号" value="${username}"/>
            <p id="usernameTip" class="errorTips"><em></em>请输入正确的手机号</p>
            <p id="serverTips" class="errorTips"><em></em></p>
          </li>
          <li>
            <span class="s-icon s-i-psw"></span>
            <input class="s-ipt" name="password" id="password"  type="password" placeholder="请输入密码"/>
            <p id="passwordTip" class="errorTips"><em></em>请输入正确的密码</p>
          </li>
        <li>
            <input name="yzm" id="code" class="f-left s-ipt" type="text"  placeholder="请输入验证码" style="width: 60%;text-indent: 10px;"/>
            <span class="verificationCode f-right"><img id="yzmImg" src="captcha.jpg" style="height: 46px" alt="验证码" onclick="refresh()"/></span>
            <p id="codeTip" class="s-tips f-left errorTips w-453" style="padding-top: 0px;"><em></em>请输入正确的验证码</p>
        </li>
          <li>
            <span class="f-left"><input name="rememberMe" id="rememberMe" value="true" type="checkbox" checked="checked">&nbsp;记住我</span>
            <a href="${sf_root}/user/forget" class="f-right getForget" style="text-decoration: none;margin-top: -4px;">忘记密码？</a>
          </li>
        </ul>
        <h3 class="mt-33"><input id="submitButton" value="立即登录" type="button" onclick="verSubmit();" class="getRegBnt"></h3>
        <p class="s-txt"><span>使用其他账号登录</span></p>
        <div class="otherLogin">
            <a href="${WeiXinClientUrl}" id="weiXinClientUrl" class="wx"></a>
            <a href="${QqClientUrl}" class="qq"></a>
            <a href="${WeiboClientUrl}" class="wb"></a>
        </div>
        <a href="${sf_root}/user/register" class="getLogin f-right"><em class="goToLoginOrReg" style="right: 66px;"></em>免费注册</a>
      </dd>
    </dl>
  </div>
</div>
</form:form>
<div class="footer">
  <div class="footerTop">
    <div class="ft-list f-left w-110">
      <h3 class="f-size16 bold">智慧会务</h3>
      <a href="${sf_root}/about">公司介绍</a>
      <a href="${sf_root}/contact">联系我们</a>
      <a href="${sf_root}/help">帮助中心</a>
    </div>
    <div class="ft-list f-left" style="width: 310px;">
      <h3 class="f-size16 bold f-left telephone-title">客服热线</h3>
      <p class="telephone">400-775-9633</p>
      <p>周一至周五 9:00 - 18:00</p>
    </div>
    <div class="cord f-right">
      <span><img src="${root}/hwLogin2017/images/zhhw-qrcode1.png"/> </span>
      <span><img src="${root}/hwLogin2017/images/zhhw-qrcode2.png"/> </span>
    </div>
  </div>
  <div class="link-container">
    <h3 class="f-size16 bold">友情链接</h3>
      <a href="http://www.hyxt.com/" target="_blank">华阳信通</a>
      <a href="http://www.bjsure.com/" target="_blank">金典永恒</a>
      <a href="http://www.pabiku.com/" target="_blank">活动策划</a>
      <a href="http://www.2dtech.cn/" target="_blank">二维科技</a>
      <a href="http://huichang.qunar.com/" target="_blank">去哪儿网-会场预订</a>
      <a href="http://www.huixiaoer.com/" target="_blank">会小二</a>
      <a href="http://www.hotelgg.com/" target="_blank">酒店哥哥</a>
      <a href="http://www.shichangbu.com/" target="_blank">市场部</a>
      <a href="http://www.wutongclub.com/" target="_blank">梧桐会</a>
      <a href="http://www.iyiou.com/" target="_blank">亿欧网</a>
  </div>
  <div class="blank30"></div>
  <div class="blank20"></div>
  <div class="footerBottom">
    <p>Copyright © 2017 深圳启会科技发展有限公司 版权所有 <a href="http://www.miitbeian.gov.cn/">粤ICP备15106759号</a></p>
  </div>
</div>
<script src="${root}/hwLogin2017/js/jquery.min.js"></script>
<script src="${root}/hwLogin/js/login.js?v=20160811"></script>
<script>
    $(function(){
        var serverErrorMsg = $.trim($("#errorMsg").html());
        if(serverErrorMsg){
            $("#serverTips").html("<em></em>"+serverErrorMsg).show()
        }
    });
</script>
</body>
</html>
