<%@ page contentType="text/html; charset=UTF-8" %>
<%
  String path = request.getContextPath();
  request.setAttribute("root",path);
//  request.setAttribute("sf_root","http://192.168.10.199/ticket");
  request.setAttribute("sf_root","http://127.0.0.1:8087");
%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <title>回味生活</title>
  <meta name="keywords" content="让生活变得更加有趣"/>
  <meta name="description" content="回味生活" />
  <link rel="stylesheet" href="${root}/liveLogin/css/reset.css"/>
  <link rel="stylesheet" href="${root}/liveLogin/css/supersized.css"/>
  <link rel="stylesheet" href="${root}/liveLogin/css/style.css"/>
  <script src="${root}/liveLogin/js/jquery-1.8.2.min.js"></script>
  <script src="${root}/liveLogin/js/supersized.3.2.7.min.js"></script>
  <script src="${root}/liveLogin/js/supersized-init.js"></script>
  <script src="${root}/liveLogin/js/scripts.js"></script>
</head>
<body style="overflow-x: hidden;background-color: #f7f8fb;">



<form:form method="post" id="loginForm" commandName="${commandName}" htmlEscape="true" novalidate="novalidate" autocomplete="off">
    <form:errors path="*" id="errorMsg" cssClass="errors" cssStyle="display: none;" element="div" htmlEscape="false" />
    <input type="hidden" name="lt" value="${loginTicket}" />
    <input type="hidden" name="execution" value="${flowExecutionKey}" />
    <input type="hidden" name="_eventId" value="submit" />
<%--<div class="pc-login-register-container">
  <div class="smallBox pb-30">
    <dl>
      <dt class="pt-30">用户登录</dt>
      <dd>
        <ul>
          <li>
            <span class="s-icon s-icon-login"></span>
            <input class="s-ipt" name="username" id="username" type="text" placeholder="请输入手机号" value="${username}"/>
            <p id="usernameTip" class="errorTips"><em></em>请输入正确的手机号</p>
            <p id="serverTips" class="errorTips"><em></em></p>
          </li>
          <li>
            <span class="s-icon s-i-psw"></span>
            <input class="s-ipt" name="password" id="password"  type="password" placeholder="请输入密码"/>
            <p id="passwordTip" class="errorTips"><em></em>请输入正确的密码</p>
          </li>
        <li>
            <input name="yzm" id="code" class="f-left s-ipt" type="text"  placeholder="请输入验证码" style="width: 60%;text-indent: 10px;"/>
            <span class="verificationCode f-right"><img id="yzmImg" src="captcha.jpg" style="height: 46px" alt="验证码" onclick="refresh()"/></span>
            <p id="codeTip" class="s-tips f-left errorTips w-453" style="padding-top: 0px;"><em></em>请输入正确的验证码</p>
        </li>
          <li>
            <span class="f-left"><input name="rememberMe" id="rememberMe" value="true" type="checkbox" checked="checked">&nbsp;记住我</span>
            <a href="${sf_root}/user/forget" class="f-right getForget" style="text-decoration: none;margin-top: -4px;">忘记密码？</a>
          </li>
        </ul>
        <h3 class="mt-33"><input id="submitButton" value="立即登录" type="button" onclick="verSubmit();" class="getRegBnt"></h3>
        <p class="s-txt"><span>使用其他账号登录</span></p>
        <div class="otherLogin">
            <a href="${WeiXinClientUrl}" id="weiXinClientUrl" class="wx"></a>
            <a href="${QqClientUrl}" class="qq"></a>
            <a href="${WeiboClientUrl}" class="wb"></a>
        </div>
        <a href="${sf_root}/user/register" class="getLogin f-right"><em class="goToLoginOrReg" style="right: 66px;"></em>免费注册</a>
      </dd>
    </dl>
  </div>
</div>--%>

  <div class="page-container">
    <h1>登录</h1>
      <input type="text" name="username" id="username" class="username" value="${username}" style="ime-mode:disabled" onpaste="return false" ondragenter="return false"   maxlength="32" placeholder="请输入昵称">
      <input type="password" name="password" id="password" class="password" maxlength="32" placeholder="请输入密码">
      <button type="button" id="btn">登录</button>
      <div class="error"><span>+</span></div>
  </div>
</form:form>


<script src="${root}/tools/layer/layer.js"></script>
<script>
    $(function(){
        $("#btn").on("click", function(event) {
            if ($(this).attr("disabled")) {
                event.preventDefault();
                return;
            }
            var username = $.trim($("#username").val());
            var password = $.trim($("#password").val());
            if (!username) {
                layer.msg("用户名不能为空!");
                document.getElementById("username").focus();
                return false;
            }
            if (!password) {
                layer.msg("用户密码不能为空");
                document.getElementById("password").focus();
                return false;
            }
            //表单提交
            $("#loginForm").trigger("submit");
        });
    });
</script>
</body>
</html>
