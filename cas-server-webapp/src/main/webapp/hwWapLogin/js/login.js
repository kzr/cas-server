var regs = {
    username: /^[1][3578][0-9]{9}$/
};
/**
 * 绑定登录事件
 */
$("#submitButton").on("click", function(event) {
    if ($(this).attr("disabled")) {
        event.preventDefault();
        return;
    }
    var uname = $.trim($("#username").val());
    var passwd = $.trim($("#password").val());
    if (!uname) {
        showErrorTip("请输入正确的手机号");
        document.getElementById("username").focus();
        return false;
    }
    if (!passwd) {
        showErrorTip("请输入正确的密码");
        document.getElementById("password").focus();
        return false;
    }
    logining();
    //表单提交
    $("#loginForm").trigger("submit");
});

/**
 * 登录中
 */
function logining() {
    $("#submitButton").val("确 认 中...").attr("disabled", true);
}

/**
 * 恢复登录
 */
function resumeLogin() {
    $("#submitButton").val("登 录").removeAttr("disabled");
}

/**
 * 显示提示信息
 * @param msg
 */
function showErrorTip(msg) {
    $("#tips").html(msg);
    $('.tipsBox').fadeIn(300).delay(2000).fadeOut();
}

$(function () {
    //绑定回车事件
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            if (!$("#submitButton").attr('disabled')) {
                $("#submitButton").click();
            }
        }
    });
    //显示登录错误结果
    document.getElementById("username").focus();
    var errorMsg = $("#errorMsg");
    if (errorMsg.size() > 0) {
        showErrorTip(errorMsg.html());
    }
});