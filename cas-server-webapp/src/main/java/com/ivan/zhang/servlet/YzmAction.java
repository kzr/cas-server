package com.ivan.zhang.servlet;

import com.ivan.zhang.RandomValidateCode;
import org.jasig.cas.CentralAuthenticationService;
import org.jasig.cas.authentication.Credential;
import org.jasig.cas.ticket.registry.TicketRegistry;
import org.jasig.cas.web.bind.CredentialsBinder;
import org.jasig.cas.web.support.WebUtils;
import org.springframework.binding.message.MessageBuilder;
import org.springframework.binding.message.MessageContext;
import org.springframework.web.util.CookieGenerator;
import org.springframework.webflow.execution.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;

/**
 * Created by Polly on 2015/1/20.
 */
public class YzmAction {

    /**
     * Authentication success result.
     */
    public static final String SUCCESS = "success";

    /**
     * Error result.
     */
    public static final String ERROR = "error";

    /**
     * Authentication failure result.
     */
    public static final String AUTHENTICATION_FAILURE = "authenticationFailure";
    /**
     * Binder that allows additional binding of form object beyond Spring
     * defaults.
     */
    private CredentialsBinder credentialsBinder;

    public CredentialsBinder getCredentialsBinder() {
        return credentialsBinder;
    }

    public void setCredentialsBinder(CredentialsBinder credentialsBinder) {
        this.credentialsBinder = credentialsBinder;
    }

    /**
     * Core we delegate to for handling all ticket related tasks.
     */
    @NotNull
    private CentralAuthenticationService centralAuthenticationService;

    /**
     * Ticket registry used to retrieve tickets by ID.
     */
    @NotNull
    private TicketRegistry ticketRegistry;

    @NotNull
    private CookieGenerator warnCookieGenerator;

    public CentralAuthenticationService getCentralAuthenticationService() {
        return centralAuthenticationService;
    }

    public void setCentralAuthenticationService(CentralAuthenticationService centralAuthenticationService) {
        this.centralAuthenticationService = centralAuthenticationService;
    }

    public TicketRegistry getTicketRegistry() {
        return ticketRegistry;
    }

    public void setTicketRegistry(TicketRegistry ticketRegistry) {
        this.ticketRegistry = ticketRegistry;
    }

    public CookieGenerator getWarnCookieGenerator() {
        return warnCookieGenerator;
    }

    public void setWarnCookieGenerator(CookieGenerator warnCookieGenerator) {
        this.warnCookieGenerator = warnCookieGenerator;
    }

    public final void doBind(final RequestContext context, final Credential credential) throws Exception {
        final HttpServletRequest request = WebUtils.getHttpServletRequest(context);

        if (this.credentialsBinder != null && this.credentialsBinder.supports(credential.getClass())) {
            this.credentialsBinder.bind(request, credential);
        }
    }

    public final String submit(final RequestContext context, final MessageContext messageContext) throws Exception {
        String yzm = context.getRequestParameters().get("yzm");
        HttpSession session = WebUtils.getHttpServletRequest(context).getSession();
        String sessionYzm = session.getAttribute(RandomValidateCode.RANDOMCODEKEY) + "";
        if (session.getAttribute("mobile") != null) {//FIXME 手机端目前不做验证码效验,但存在风险
            return SUCCESS;
        }
        if (!sessionYzm.equalsIgnoreCase(yzm)) {
            MessageBuilder msgBuilder = new MessageBuilder();
            msgBuilder.defaultText("验证码错误！");
            messageContext.addMessage(msgBuilder.error().build());
            return ERROR;
        }
        return SUCCESS;
    }
}
