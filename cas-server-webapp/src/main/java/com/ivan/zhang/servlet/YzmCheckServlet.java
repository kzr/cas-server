package com.ivan.zhang.servlet;

import com.ivan.zhang.RandomValidateCode;
import org.jasig.cas.web.support.WebUtils;
import org.springframework.binding.message.MessageBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Polly on 2015/1/29.
 */
public class YzmCheckServlet extends HttpServlet {

	public void destroy() {
		super.destroy();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0L);
		response.setContentType("text/html; charset=UTF-8");
		String yzm = request.getParameter("yzm");
		String sessionYzm = request.getSession().getAttribute(RandomValidateCode.RANDOMCODEKEY).toString();

		PrintWriter out = response.getWriter();

		if (!sessionYzm.equalsIgnoreCase(yzm)) {
			out.print("0");
		} else {
			out.print("1");
		}
		out.flush();
		out.close();
	}

	public void init()
			throws ServletException {
	}
}
