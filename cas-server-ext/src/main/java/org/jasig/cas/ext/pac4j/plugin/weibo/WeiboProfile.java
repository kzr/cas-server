package org.jasig.cas.ext.pac4j.plugin.weibo;

import org.jasig.cas.ext.pac4j.plugin.profile.OAuthAttributesDefinitions;
import org.pac4j.core.profile.AttributesDefinition;
import org.pac4j.oauth.profile.OAuth20Profile;

/**
 * Created by rain.wen on 2016/9/2.
 */
public class WeiboProfile extends OAuth20Profile {

    private static final long serialVersionUID = -7969484323692570444L;

    @Override
    protected AttributesDefinition getAttributesDefinition() {
        return OAuthAttributesDefinitions.weiboDefinition;
    }
}
