package org.jasig.cas.ext.pac4j.plugin.common;

/**
 * Created by rain.wen on 2016/9/2.
 */
public enum ClientType {
    WECHAT,
    WEIBO,
    QQ
}
