package org.jasig.cas.ext.pac4j.plugin.weibo;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.model.OAuthConfig;
import org.scribe.oauth.ProxyOAuth20ServiceImpl;

/**
 * Created by rain.wen on 2016/9/2.
 */
public class WeiboOAuth20ServiceImpl extends ProxyOAuth20ServiceImpl {

    public WeiboOAuth20ServiceImpl(DefaultApi20 api, OAuthConfig config, int connectTimeout, int readTimeout, String proxyHost, int proxyPort) {
        super(api, config, connectTimeout, readTimeout, proxyHost, proxyPort);
    }


}
