package org.jasig.cas.ext.pac4j.plugin.qq;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.extractors.JsonTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.Verb;
import org.scribe.utils.OAuthEncoder;

import java.util.UUID;

/**
 * 用于定义获取微信返回的CODE与ACCESS_TOKEN
 *
 * @author b2c021
 */
public class QqApi20 extends DefaultApi20 {

    public final static String BASE_URL = "https://graph.qq.com/";

    public final static String ACCESS_TOKEN_URL = BASE_URL + "oauth2.0/token";

    public final static String AUTHORIZATION_URL = BASE_URL
            + "oauth2.0/authorize?client_id=%s&redirect_uri=%s&response_type=code&state=%s";

    @Override
    public String getAccessTokenEndpoint() {
        return ACCESS_TOKEN_URL;
    }

    @Override
    public String getAuthorizationUrl(OAuthConfig config) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");

        if (config.hasScope()) {
            return String.format(AUTHORIZATION_URL + "&scope=%s", config.getApiKey(),
                    OAuthEncoder.encode(config.getCallback()), uuid,
                    OAuthEncoder.encode(config.getScope()));
        } else {
            return String.format(AUTHORIZATION_URL, config.getApiKey(),
                    OAuthEncoder.encode(config.getCallback()), uuid);
        }
    }


}
