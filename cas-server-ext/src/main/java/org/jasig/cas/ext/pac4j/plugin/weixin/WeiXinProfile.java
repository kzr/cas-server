package org.jasig.cas.ext.pac4j.plugin.weixin;

import org.jasig.cas.ext.pac4j.plugin.profile.OAuthAttributesDefinitions;
import org.pac4j.core.profile.AttributesDefinition;
import org.pac4j.oauth.profile.OAuth20Profile;

/**
 * 用于添加返回用户信息
 * @author wenjl
 *
 */
public class WeiXinProfile extends OAuth20Profile {

    private static final long serialVersionUID = -7969484323692570444L;

    @Override
    protected AttributesDefinition getAttributesDefinition() {
        return OAuthAttributesDefinitions.weixinDefinition;
    }

}
