package org.jasig.cas.ext.pac4j.plugin.weixin;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.extractors.JsonTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.Verb;
import org.scribe.utils.OAuthEncoder;

/**
 * 用于定义获取微信返回的CODE与ACCESS_TOKEN
 * @author wenjl
 *
 */
public class WeiXinApi20 extends DefaultApi20 {

    //微信二维码扫描授权
//    private static final String AUTHORIZE_URL = "https://open.weixin.qq.com/connect/qrconnect?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_login#wechat_redirect";
    //微信公众号授权
    private static final String AUTHORIZE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_login#wechat_redirect";

    @Override
    public AccessTokenExtractor getAccessTokenExtractor()
    {
      return new JsonTokenExtractor();
    }

    @Override
    public Verb getAccessTokenVerb()
    {
      return Verb.POST;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return "https://api.weixin.qq.com/sns/oauth2/access_token";
    }

    @Override
    public String getAuthorizationUrl(OAuthConfig config) {
        return String.format(AUTHORIZE_URL, config.getApiKey(), OAuthEncoder.encode(config.getCallback()));
    }

}
