package org.jasig.cas.ext.pac4j.plugin.weibo;

import com.fasterxml.jackson.databind.JsonNode;
import org.jasig.cas.ext.pac4j.plugin.profile.OAuthAttributesDefinitions;
import org.jasig.cas.ext.pac4j.plugin.qq.QqAttributesDefinition;
import org.pac4j.core.client.BaseClient;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.profile.UserProfile;
import org.pac4j.oauth.client.BaseOAuth20Client;
import org.pac4j.oauth.credentials.OAuthCredentials;
import org.pac4j.oauth.profile.JsonHelper;
import org.scribe.model.OAuthConfig;
import org.scribe.model.SignatureType;

import java.util.List;

/**
 * 登录的入口会根据 WeiboClient创建对应 WeiboClientUrl
 * Created by rain.wen on 2016/9/2.
 */
public class WeiboClient extends BaseOAuth20Client<WeiboProfile> {

    /**
     * 是否需要state
     * @return
     */
    @Override
    protected boolean requiresStateParameter() {
        return false;
    }

    @Override
    protected boolean hasBeenCancelled(WebContext webContext) {
        return false;
    }

    @Override
    protected String getProfileUrl() {
        return WeiboApi20.BASE_URL + "2/statuses/user_timeline.json";
    }

    @Override
    protected WeiboProfile extractUserProfile(String body) {
        final WeiboProfile profile = new WeiboProfile();
        JsonNode json = JsonHelper.getFirstNode(body);

        if (json != null) {
            JsonNode statuses = json.get("statuses");

            if (statuses != null) {
                JsonNode status = statuses.get(0);

                if (status != null) {
                    JsonNode user = status.get("user");

                    if (user != null) {
                        profile.setId(JsonHelper.get(user, WeiboAttributesDefinition.ID));

                        List<String> principalAttributes = OAuthAttributesDefinitions.weiboDefinition
                                .getPrincipalAttributes();
                        for (final String name : principalAttributes) {
                            Object value = JsonHelper.get(user, name);

                            profile.addAttribute(name, value);
                            if (WeiboAttributesDefinition.SCREEN_NAME.equals(name) == true) {
                                profile.addAttribute(WeiboAttributesDefinition.USERNAME, value);
                            } else if (WeiboAttributesDefinition.LOCATION.equals(name) == true) {
                                profile.addAttribute(WeiboAttributesDefinition.ADDRESS, value);
                            }
                        }

                        Object avatarHd = JsonHelper.get(user, WeiboAttributesDefinition.AVATAR_HD);
                        if (avatarHd != null && avatarHd.toString().length() != 0) {
                            profile.addAttribute(WeiboAttributesDefinition.AVATAR, avatarHd);
                            profile.addAttribute(QqAttributesDefinition.PICTURE_URL, avatarHd);
                        } else {
                            Object avatarLarge = JsonHelper.get(user,
                                    WeiboAttributesDefinition.AVATAR_LARGE);
                            if (avatarLarge != null && avatarLarge.toString().length() != 0) {
                                profile.addAttribute(WeiboAttributesDefinition.AVATAR, avatarLarge);
                                profile.addAttribute(QqAttributesDefinition.PICTURE_URL, avatarLarge);
                            }
                        }
                    }
                }
            }
        }
        return profile;
    }

    @Override
    protected BaseClient<OAuthCredentials, WeiboProfile> newClient() {
        WeiboClient newClient = new WeiboClient();
        return newClient;
    }

    @Override
    protected void internalInit() {
        super.internalInit();
        WeiboApi20 api = new WeiboApi20();
        this.service = new WeiboOAuth20ServiceImpl(api, new OAuthConfig(this.key, this.secret, this.callbackUrl, SignatureType.Header, null, null),
                this.connectTimeout, this.readTimeout, this.proxyHost,this.proxyPort);
    }
}