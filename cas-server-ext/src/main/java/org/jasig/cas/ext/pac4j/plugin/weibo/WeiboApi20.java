package org.jasig.cas.ext.pac4j.plugin.weibo;

import org.scribe.builder.api.DefaultApi20;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.extractors.JsonTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.Verb;
import org.scribe.utils.OAuthEncoder;

/**
 * Created by rain.wen on 2016/9/2.
 */
public class WeiboApi20 extends DefaultApi20 {

    public final static String BASE_URL = "https://api.weibo.com/";

    private static final String AUTHORIZE_URL = "https://api.weibo.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code";

    private static final String SCOPED_AUTHORIZE_URL = "https://api.weibo.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s";


    public Verb getAccessTokenVerb() {
        return Verb.POST;
    }

    public AccessTokenExtractor getAccessTokenExtractor() {
        return new JsonTokenExtractor();
    }

    public String getAccessTokenEndpoint() {
        return "https://api.weibo.com/oauth2/access_token?grant_type=authorization_code";
    }

    public String getAuthorizationUrl(OAuthConfig config) {
        return config.hasScope() ? String.format("https://api.weibo.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code&scope=%s",
                new Object[]{config.getApiKey(), OAuthEncoder.encode(config.getCallback()),
                        OAuthEncoder.encode(config.getScope())}) :
                String.format("https://api.weibo.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code",
                        new Object[]{config.getApiKey(), OAuthEncoder.encode(config.getCallback())});
    }
}

