package org.jasig.cas.adaptors.exception;

import javax.security.auth.login.AccountException;

/**
 * Created by Polly on 2015/1/26.
 */
public class YzmErrorException extends AccountException {

	public YzmErrorException() {
		super();
	}

	public YzmErrorException(final String msg) {
		super(msg);
	}
}
