package org.jasig.cas.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by user on 2015/6/29.
 */
public class PropertiesUtils {
    private static Properties pro  ;

    static  {
        try {
            pro = new Properties();
            String path = getPathWebInf();
            FileInputStream in = new FileInputStream(path+ File.separator +"cas.properties");
            pro.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int getPropertyInt(String key){
        String o = pro.getProperty(key);
        if(o!=null){
            return Integer.valueOf(o.toString()) ;
        }else
            return 0;

    }

    public static String getProperty(String key){
        String property = pro.getProperty(key);
        return property ;
    }

    public static String getPathWebInf(){
        String path = PropertiesUtils.class.getClassLoader().getResource("").getPath().replace("%20", " ");
        if(path.contains(":")){
            return path.substring(1, path.indexOf("classes"));
        }else {
            return path.substring(0, path.indexOf("classes"));
        }
    }
}
